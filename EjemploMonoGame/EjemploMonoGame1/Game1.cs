﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace EjemploMonoGame1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Pelotita mi_sprite;
        Personaje personaje;
        EnemigoPerseguidor enemigo;
        EnemigoPruebaEstados prueba_Estados;
        List<EnemigoPerseguidor> perseguidores;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            mi_sprite = new Pelotita();
            personaje = new Personaje();
            enemigo = new EnemigoPerseguidor();

            perseguidores = new List<EnemigoPerseguidor>();
            for (int i=0; i<50; i++)
            {
                perseguidores.Add(new EnemigoPerseguidor());
            }

            prueba_Estados = new EnemigoPruebaEstados();

            base.Initialize();
        }


        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            mi_sprite.inicializar(Content.Load<Texture2D>("gote"), new Vector2(10, 100),new Vector2(3,3));
            personaje.inicializar(Content.Load<Texture2D>("normal"),new Vector2(700,1), new Vector2(5,5));
            enemigo.inicializar(Content.Load<Texture2D>("veg"),new Vector2(1,1), new Vector2(1,1),new Vector2(1,1));

            for (int i=0; i<perseguidores.Count; i++)
            {
               perseguidores[i].inicializar(Content.Load<Texture2D>("veg"), new Vector2(300, 200), new Vector2(0, 0), new Vector2(0.005f * (i+1), 0.005f * (i+1)));
            }

            prueba_Estados.inicializar(Content.Load<Texture2D>("gokus7"), new Vector2(300,200), new Vector2(10,10));

            // TODO: use this.Content to load your game content here
        }

            /// <summary>
            /// UnloadContent will be called once per game and is the place to unload
            /// game-specific content.
            /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            mi_sprite.actualizar();
            personaje.actualizar(Keyboard.GetState());
            enemigo.actualizar(personaje);

            for (int i=0; i<perseguidores.Count; i++)
            {
                perseguidores[i].actualizar(personaje);
            }

            if (personaje.estaColisionando(enemigo))
            {
                
                if (personaje.grafico.Name == "gokus88")
                {
                    personaje.cambiarSprite(Content.Load<Texture2D>("normal"));
                }
                else
                {
                    personaje.cambiarSprite(Content.Load<Texture2D>("gokus88"));
                }
               
            }

            prueba_Estados.actualizar();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            mi_sprite.dibujar(spriteBatch);
            personaje.dibujar(spriteBatch);
            enemigo.dibujar(spriteBatch);

            for (int i=0; i<perseguidores.Count;i++)
            {
                perseguidores[i].dibujar(spriteBatch);
            }

            prueba_Estados.dibujar(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
