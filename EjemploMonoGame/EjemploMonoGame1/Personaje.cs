﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace EjemploMonoGame1
{
    class Personaje : Sprite
    {
        public void actualizar(KeyboardState teclado)
        {
            if (teclado.IsKeyDown(Keys.Right))
            {
                posicion.X += velocidad.X;
            }

            if (teclado.IsKeyDown(Keys.Left))
            {
                posicion.X -= velocidad.X;
            }

            if (teclado.IsKeyDown(Keys.Down))
            {
                posicion.Y += velocidad.Y;
            }

            if (teclado.IsKeyDown(Keys.Up))
            {
                posicion.Y -= velocidad.Y;
            }

        }

        public void cambiarSprite(Texture2D nuevoSprite)
        {
            this.grafico = nuevoSprite;
        }
    }
}
