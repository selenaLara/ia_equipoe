﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EjemploMonoGame1
{
    class EnemigoPerseguidor : Sprite
    {
       
        public void actualizar(Sprite personaje)
        {
            if (personaje.posicion.X < posicion.X)
            {
                velocidad.X -= aceleracion.X;
            }

            if (personaje.posicion.X > posicion.X)
            {
                velocidad.X += aceleracion.X;
            }

            if (personaje.posicion.Y < posicion.Y)
            {
                velocidad.Y -= aceleracion.Y;
            }

            if (personaje.posicion.Y > posicion.Y)
            {
                velocidad.Y += aceleracion.Y;
            }
            
            if (velocidad.X > 1)
            {
                velocidad.X = 1;
            }

            if (velocidad.X < -1)
            {
                velocidad.X = -1;
            }

            if (velocidad.Y > 1)
            {
                velocidad.Y = 1;
            }

            if (velocidad.Y < -1)
            {
                velocidad.Y= -1;
            }

            posicion += velocidad;
        }
    }
}
