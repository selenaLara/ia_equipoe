﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EjemploMonoGame1
{
    class Sprite
    {
        public Vector2 posicion;
        public Texture2D grafico;
        public Vector2 velocidad, aceleracion;

        public void inicializar(Texture2D grafico, Vector2 posicion)
        {
            this.posicion = posicion;
            this.grafico = grafico;
        }



        public void inicializar(Texture2D grafico, Vector2 posicion, Vector2 velocidad, Vector2 aceleracion)
        {
            this.grafico = grafico;
            this.posicion = posicion;
            this.velocidad = velocidad;
            this.aceleracion = aceleracion;
        }


        public void inicializar(Texture2D grafico, Vector2 posicion, Vector2 velocidad)
        {
            this.grafico = grafico;
            this.posicion = posicion;
            this.velocidad = velocidad;
        }



        public void dibujar(SpriteBatch dibujador)
        {
            dibujador.Draw(grafico,posicion,Color.White);
        }

        public void actualizar()
        {
            posicion.X--;
        }

        public bool estaColisionando(Sprite elOtroSprite)
        {
            

            //Si el otro sprite está a la izquierda
            if ((elOtroSprite.posicion.X + elOtroSprite.grafico.Width) < posicion.X)
            {
                return false;
            }
            //Si el otro esta a la derecha
            else  if ((posicion.X + grafico.Width) < elOtroSprite.posicion.X)
            {
                return false;
            }
            //Si esta abajo
            else if ((elOtroSprite.posicion.Y + elOtroSprite.grafico.Height) < posicion.Y)
            {
                return false;
            }
            //Si esta arriba
            else if ((posicion.Y + grafico.Height) < elOtroSprite.posicion.Y)
            {
                return false;
            }

            return true;
        }


    }
}
