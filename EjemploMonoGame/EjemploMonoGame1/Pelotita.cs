﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EjemploMonoGame1
{
    class Pelotita : Sprite
    {

        public void actualizar()
        {
            posicion += velocidad;

            if (posicion.X > 700 || posicion.X<0)
            {
                velocidad.X *= -1;
            }

            if (posicion.Y > 450 || posicion.Y<0)
            {
                velocidad.Y *= -1;
            }
        }
    }
}
