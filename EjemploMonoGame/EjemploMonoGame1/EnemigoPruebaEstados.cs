﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace EjemploMonoGame1
{
    class EnemigoPruebaEstados : Sprite
    {
        public  EstadoEnemigoPruebaEstados estadoActual;

        public EnemigoPruebaEstados()
        {
            estadoActual = new EstadoIrHaciaDerechaEnemigoPruebaEstados();
        }

        public void actualizar()
        {
            estadoActual.procesar(this);
        }
    }

    class EstadoEnemigoPruebaEstados
    {
        public virtual void procesar(EnemigoPruebaEstados llamador)
        {

        }
    }

    class EstadoIrHaciaDerechaEnemigoPruebaEstados : EstadoEnemigoPruebaEstados
    {
        public override void procesar(EnemigoPruebaEstados llamador)
        {
            llamador.posicion.X += llamador.velocidad.X;

            if (llamador.posicion.X > 720)
            {
                llamador.estadoActual = new EstadoIrHaciaIzquierdaEnemigoPruebaEstados();
            }
        }
    }

    class EstadoIrHaciaIzquierdaEnemigoPruebaEstados : EstadoEnemigoPruebaEstados
    {
        public override void procesar(EnemigoPruebaEstados llamador)
        {
            llamador.posicion.X -= llamador.velocidad.X;

            if (llamador.posicion.X < 0)
            {
                llamador.estadoActual = new EstadoIrHaciaDerechaEnemigoPruebaEstados();
            }
        }
    }
}
