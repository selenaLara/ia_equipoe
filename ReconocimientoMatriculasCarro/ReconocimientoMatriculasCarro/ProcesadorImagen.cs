﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace ReconocimientoMatriculasCarro
{
    public class ProcesadorImagen
    {
        int[,] pixeles;
        int[,] binario;
        public static int[,] etiqueta;
        ComponenteConectada componenteConectada;
        int[,] mapaDeBits;
        int[,] etiquetaCopia;
        int contadorIteraciones = 0;

        public ProcesadorImagen(Bitmap mapaDeBits)
        {
        //   this.mapaDeBits = new Bitmap(mapaDeBits.Width,mapaDeBits.Height,System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            pixeles = new int[mapaDeBits.Width, mapaDeBits.Height];

            for (int j=0; j<mapaDeBits.Height; j++)
            {
                for (int i=0; i< mapaDeBits.Width; i++)
                {
                    pixeles[i, j] = mapaDeBits.GetPixel(i, j).ToArgb();
                }
            }
        }

        public void binarizar(int umbral = 127)
        {
            int rojo, verde, azul, gris;
            binario = new int[pixeles.GetLength(0), pixeles.GetLength(1)]; 

            for (int j=0; j<pixeles.GetLength(1);j++)
            {
                for (int i=0; i<pixeles.GetLength(0); i++)
                {
                    rojo = (pixeles[i, j] & 0xff0000) >> 16;
                    verde = (pixeles[i, j] & 0xff00) >> 8;
                    azul = pixeles[i, j] & 0xff;
                    gris = (rojo + verde + azul) / 3;


                    if (gris > umbral)
                    {
                        pixeles[i, j] = 255;
                        binario[i, j] = 0;
                    }
                    else
                    {
                        binario[i, j] = 1;
                        pixeles[i, j] = 0;
                    }

                }
            }

        }
        
        public Bitmap GetMapaDeBitsBinario()
        {
            Bitmap mapa_Binario = new Bitmap (pixeles.GetLength(0), pixeles.GetLength(1), System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            for (int j=0; j<pixeles.GetLength(1); j++)
            {
                for (int i=0; i<pixeles.GetLength(0); i++)
                {
                    mapa_Binario.SetPixel(i,j,Color.FromArgb((255<<24) | (pixeles[i,j] << 16) | (pixeles[i,j] << 8) | pixeles[i,j]));
                }
            }

            return mapa_Binario;
        }
        
        public void etiquetadoDeComponentesConexas()
        {

            igualarMatrices(etiqueta,etiquetaCopia);

            for (int j = 0; j < pixeles.GetLength(1); j++)
            {
                for (int i = 0; i < pixeles.GetLength(0); i++)
                {
                    if (binario[i, j] == 1)
                    {

                        if (i - 1 >= 0)
                        {
                            if (etiqueta[i, j] > etiqueta[i - 1, j] && etiqueta[i - 1, j] != 0)
                            {
                                etiqueta[i, j] = etiqueta[i - 1, j];
                            }
                        }

                        if (j - 1 >= 0)
                        {
                            if (etiqueta[i, j] > etiqueta[i, j - 1] && etiqueta[i, j - 1] != 0)
                            {
                                etiqueta[i, j] = etiqueta[i, j - 1];
                            }
                        }

                        if (i + 1 < pixeles.GetLength(0) - 1)
                        {
                            if (etiqueta[i, j] > etiqueta[i + 1, j] && etiqueta[i + 1, j] != 0)
                            {
                                etiqueta[i, j] = etiqueta[i + 1, j];
                            }
                        }

                        if (j + 1 < pixeles.GetLength(1) - 1)
                        {
                            if (etiqueta[i, j] > etiqueta[i, j + 1] && etiqueta[i, j + 1] != 0)
                            {
                                etiqueta[i, j] = etiqueta[i, j + 1];
                            }

                        }
                        

                    }
                    Console.Write("[" + etiqueta[i, j] + "] ");
                }
               Console.WriteLine("");
            }

            etiquetadoInverso();

            if (contadorIteraciones < 1)
            {
                if (etiqueta != etiquetaCopia)
                {
                    contadorIteraciones++;
                    etiquetadoDeComponentesConexas();
                }
            }
            


        } 

        public void etiquetadoContador()
        {
            etiqueta = new int[binario.GetLength(0), binario.GetLength(1)];
            etiquetaCopia = new int[binario.GetLength(0), binario.GetLength(1)];
            int cont = 2;

            for (int j = 0; j < pixeles.GetLength(1); j++)
            {
                for (int i = 0; i < pixeles.GetLength(0); i++)
                {
                    if (binario[i, j] == 1)
                    {
                        etiqueta[i, j] = cont;
                        cont++;
                    }
                    //Console.Write("[" + etiqueta[i, j] + "] ");
                }
              // Console.WriteLine("");
            }

            etiquetadoDeComponentesConexas();
        }

        public void igualarMatrices(int[,] etiqueta, int[,] etiquetaCopia)
        {
            for (int j = 0; j < pixeles.GetLength(1); j++)
            {
                for (int i = 0; i < pixeles.GetLength(0); i++)
                {
                    etiquetaCopia[i, j] = etiqueta[i, j];
                }
            }
        }
        public void etiquetadoInverso()
        {            
            for (int j = pixeles.GetLength(1)-1; j > 0; j--)
            {
                for (int i = pixeles.GetLength(0)-1; i > 0; i--)
                {
                    if (binario[i, j] == 1)
                    {
                        
                        if (i + 1 < pixeles.GetLength(0) - 1)
                        {
                            if (etiqueta[i, j] > etiqueta[i + 1, j] && etiqueta[i + 1, j] != 0)
                            {
                                etiqueta[i, j] = etiqueta[i + 1, j];
                            }
                        }

                        if (j + 1 < pixeles.GetLength(1) - 1)
                        {
                             if (etiqueta[i, j] > etiqueta[i, j + 1] && etiqueta[i, j + 1] != 0)
                            {
                                etiqueta[i, j] = etiqueta[i, j + 1];
                            }
                            
                        }

                        if (i - 1 >= 0)
                        {
                            if (etiqueta[i, j] > etiqueta[i - 1, j] && etiqueta[i - 1, j] != 0)
                            {
                                etiqueta[i, j] = etiqueta[i - 1, j];
                            }
                        }

                        if (j - 1 >= 0)
                        {
                            if (etiqueta[i, j] > etiqueta[i, j - 1] && etiqueta[i, j - 1] != 0)
                            {
                                etiqueta[i, j] = etiqueta[i, j - 1];
                            }
                        }
                        
                    }
                    //Console.Write("[" + etiqueta[i, j] + "] ");
                }
              //  Console.WriteLine("");
            }
        }

        public void segmentacionDeComponentesConexas()
        {
            componenteConectada = new ComponenteConectada();
            componenteConectada.inicializarDiccionario(etiqueta);

        }

    }
}
