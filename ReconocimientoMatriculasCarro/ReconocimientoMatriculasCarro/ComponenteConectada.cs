﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ReconocimientoMatriculasCarro
{
    class ComponenteConectada
    {
        int XDerecha, XIzquierda, YArriba , YAbajo;
        int[,] pixeles;
        Dictionary<int, ComponenteConectada> diccionarioComponentes = new Dictionary<int, ComponenteConectada>();

        public ComponenteConectada(int X, int Y)
        {
            XDerecha = X;
            XIzquierda = X;
            YArriba = Y;
            YAbajo = Y;
        }

        public ComponenteConectada()
        {

        }
        

        public void actualizarPosiciones(int posX, int posY)
        {
            if (posX  < XIzquierda)
            {
                XIzquierda = posX;
            }
            if (posX > XDerecha)
            {
                XDerecha = posX;
            }

            if (posY  < YArriba)
            {
                YArriba = posY;
            }

            if (posY > YAbajo)
            {
                YAbajo = posY;
            }
        }

        public void inicializarDiccionario(int[,] etiqueta)
        {
            int identificador;
            diccionarioComponentes.Add(0, new ReconocimientoMatriculasCarro.ComponenteConectada());

            for(int j=0; j<etiqueta.GetLength(0);j++)
            {
                for (int i = 0; i < etiqueta.GetLength(0); i++)
                {
                    if (etiqueta[i,j] != 0)
                    {
                        identificador = etiqueta[i, j]; 

                            if (diccionarioComponentes.ContainsKey(identificador))
                            {
                                diccionarioComponentes[identificador].actualizarPosiciones(i,j);
                            }
                            else
                            {
                                diccionarioComponentes.Add(identificador, new ComponenteConectada(i,j));
                            }
                        
                    }
                }
            }
            imprimirDiccionario();
            replicarMatriz(etiqueta);
        }

        public void imprimirDiccionario()
        {
            for (int i=0; i<diccionarioComponentes.Keys.Count;i++)
            {
                if (diccionarioComponentes.Keys.ElementAt(i)!=0)
                {
                    Console.WriteLine("La etiqueta " + diccionarioComponentes.Keys.ElementAt(i) + " tiene en X Derecha" + diccionarioComponentes[diccionarioComponentes.Keys.ElementAt(i)].XDerecha);
                    Console.WriteLine("La etiqueta " + diccionarioComponentes.Keys.ElementAt(i) + " tiene en X Izquierda" + diccionarioComponentes[diccionarioComponentes.Keys.ElementAt(i)].XIzquierda);
                    Console.WriteLine("La etiqueta " + diccionarioComponentes.Keys.ElementAt(i) + " tiene en Y Arriba" + diccionarioComponentes[diccionarioComponentes.Keys.ElementAt(i)].YArriba);
                    Console.WriteLine("La etiqueta " + diccionarioComponentes.Keys.ElementAt(i) + " tiene en Y Abajo" + diccionarioComponentes[diccionarioComponentes.Keys.ElementAt(i)].YAbajo + "\n");
                }
            }
        }

        public void replicarMatriz(int[,]original)
        {

            for (int m=0; m<diccionarioComponentes.Keys.Count;m++)
            {
                if (diccionarioComponentes.Keys.ElementAt(m)!=0)
                {
                    int XRight = diccionarioComponentes[diccionarioComponentes.Keys.ElementAt(m)].XDerecha;
                    int XLeft = diccionarioComponentes[diccionarioComponentes.Keys.ElementAt(m)].XIzquierda;
                    int YUp = diccionarioComponentes[diccionarioComponentes.Keys.ElementAt(m)].YArriba;
                    int YDown = diccionarioComponentes[diccionarioComponentes.Keys.ElementAt(m)].YAbajo;


                    int dimensionX = (XRight - XLeft) + 1;
                    int dimensionY = (YDown - YUp) + 1;

                    if (dimensionX > 7 && dimensionY > 15)
                    {
                        if (dimensionX/dimensionY > 0.3)
                        {
                            Console.WriteLine("\n Matriz del elemento " + diccionarioComponentes.Keys.ElementAt(m) + " : ");
                            pixeles = new int[dimensionX, dimensionY];

                            for (int j = YUp; j < YDown; j++)
                            {
                                for (int i = XLeft; i < XRight; i++)
                                {
                                    pixeles[i - XLeft, j - YUp] = original[i, j];
                                    Console.Write(pixeles[i - XLeft, j - YUp] + " ");
                                }
                                Console.WriteLine(" ");
                            }
                        }
                           

                        
                    }
                                  
                }                
            }            
        }
    }

    

}
