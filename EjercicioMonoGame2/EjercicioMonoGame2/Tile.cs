﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EjercicioMonoGame2
{
    public class Tile : Sprite
    {
        Vector2 PosicionTile;
        public const int ANCHURA_TILE = 32;
        public const int ALTURA_TILE = 32;
        public const int TIPO_TILE_AGUA = 0;
        public const int TIPO_TILE_PASTO = 1;
        public const int TIPO_TILE_TIERRA = 2;
        public int TipoTile;
        public int CostoMovimiento;

        public void Inicializar(Texture2D Grafico, Vector2 PosicionTile, int TipoTile)
        {
            this.Grafico = Grafico;
            this.PosicionTile = PosicionTile;
            this.TipoTile = TipoTile;
            PosicionAbsoluta = new Vector2(PosicionTile.X * ANCHURA_TILE, PosicionTile.Y * ALTURA_TILE);
        }
    }
}
