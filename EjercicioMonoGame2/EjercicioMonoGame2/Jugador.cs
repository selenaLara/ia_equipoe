﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace EjercicioMonoGame2
{
    public class Jugador : Sprite
    {
        public void Procesate(KeyboardState Teclado)
        {
            if(Teclado.IsKeyDown(Keys.Left))
            {
                PosicionAbsoluta.X += Velocidad.X;
            }
            if (Teclado.IsKeyDown(Keys.Right))
            {
                PosicionAbsoluta.X -= Velocidad.X;
            }
            if (Teclado.IsKeyDown(Keys.Up))
            {
                PosicionAbsoluta.Y += Velocidad.Y;
            }
            if (Teclado.IsKeyDown(Keys.Down))
            {
                PosicionAbsoluta.Y -= Velocidad.Y;
            }
        }

        public void Dibujate()
        {
            Vector2 PosicionDibujado = new Vector2(400, 200);
            DatosJuego.Dibujador.Draw(Grafico, PosicionDibujado, Color.White);
        }
    }
}
