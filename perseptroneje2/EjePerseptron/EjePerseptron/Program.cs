﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjePerseptron
{
    class Program
    {
        static void Main(string[] args)
        {
            //string entrada = "";

            //Console.WriteLine("Ingresar pesos (y/n)");
            //entrada = Convert.ToString(Console.ReadLine());

            
            
                Random aleatorio = new Random();
                Perceptron p = new Perceptron(2, aleatorio, 0.5f);
                bool sw = false;
                while (!sw)
                {
                    sw = true;
                
                    Console.WriteLine("---------------ITERACIONES-------------------");
                
                Console.WriteLine("Peso 1: " + p.wp[0]);
                Console.WriteLine("Peso 2: " + p.wp[1]);
                    Console.WriteLine("bias: " + p.Umbral);


                Console.WriteLine("E1:1 E2:1 : " + p.sfinal(new float[2] { 1f, 1f }));
                    Console.WriteLine("E1:1 E2:0 : " + p.sfinal(new float[2] { 1f, 0f }));
                    Console.WriteLine("E1:0 E2:1 : " + p.sfinal(new float[2] { 0f, 1f }));
                    Console.WriteLine("E1:0 E2:0 : " + p.sfinal(new float[2] { 0f, 0f }));
                    //peso = pesoanterior + tasa aprendizaje * error * entrada 
                    if (p.sfinal(new float[2] { 1f, 1f }) != 1)
                    {

                        p.Aprender(new float[2] { 1f, 1f }, 1);
                        sw = false;

                    }
                    if (p.sfinal(new float[2] { 1f, 0f }) != 0)
                    {

                        p.Aprender(new float[2] { 1f, 0f }, 0);
                        sw = false;

                    }
                    if (p.sfinal(new float[2] { 0f, 1f }) != 0)
                    {

                        p.Aprender(new float[2] { 0f, 1f }, 0);
                        sw = false;

                    }
                    if (p.sfinal(new float[2] { 0f, 0f }) != 0)
                    {

                        p.Aprender(new float[2] { 0f, 0f }, 0);
                        sw = false;
                    }
                }
                Console.ReadKey();
            }

        
    }
               
 }



