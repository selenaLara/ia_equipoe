﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjePerseptron
{
    class Perceptron
    {
        float[] PesosAnteriores;
        float UmbralAnterior;
        //peso = pesoanterior + tasa aprendizaje * error * entrada 
        public float[] wp;
        public float Umbral;
        public float TasaDeAprendizaje = 0.3f;
        Random r;
        public Perceptron(int NEntradas, Random R, float tasaDeAprendizaje = 0.3f)
        {

            r = R;
            TasaDeAprendizaje = tasaDeAprendizaje;
            wp = new float[NEntradas];
            PesosAnteriores = new float[NEntradas];
            Aprender();

        }
        public void Aprender()
        {
            for (int i = 0; i < PesosAnteriores.Length; i++)
            {
                PesosAnteriores[i] = Convert.ToSingle(2 * r.NextDouble() - 1);
            }
            UmbralAnterior = Convert.ToSingle(2 * r.NextDouble() - 1);

            wp = PesosAnteriores;
            Umbral = UmbralAnterior;
        }
        public void Aprender(float[] entradas, float salidaEsperada)
        {
            float error = salidaEsperada - sfinal(entradas);
            for (int i = 0; i < wp.Length; i++)
            {
                wp[i] = PesosAnteriores[i] + TasaDeAprendizaje * error * entradas[i];
            }
            Umbral = UmbralAnterior + TasaDeAprendizaje * error;

            PesosAnteriores = wp;
            UmbralAnterior = Umbral;
        }
        public float sfinal(float[] entradas)
        {
            float sum = Umbral;
            for (int i = 0; i < wp.Length; i++)
            {
                sum += entradas[i] * wp[i];
            }
            return Sigmoid(sum);
        }
        float Sigmoid(float d)
        {
            return d > 0 ? 1 : 0;
        }
    



}
}
