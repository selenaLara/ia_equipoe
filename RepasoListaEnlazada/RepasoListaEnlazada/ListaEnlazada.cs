﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoListaEnlazada
{
    class ListaEnlazada
    {
        public Nodo Raiz = null
            ;
        public void AgregarNodo( Nodo NodoNuevo)
        {
            if (Raiz == null)
            {
                Raiz = NodoNuevo;
            }
            else
            {
                Nodo nodo_auxiliar = Raiz;
                while (nodo_auxiliar.siguiente != null)
                    nodo_auxiliar = nodo_auxiliar.siguiente;
                nodo_auxiliar.siguiente = NodoNuevo;
            }

        }
        public string GetDatosLista() {
            Nodo auxiliar;
            auxiliar = Raiz;
            string datos = "";
            while(auxiliar != null)
            {
                datos += auxiliar.Nomnbre + "" + auxiliar.Numero + "\n";
                auxiliar = auxiliar.siguiente;
            }
            return datos;


        }
    }
}
