﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGameEjemplo
{
    class EnemigoPruebaEstados : Sprite
    {
        public EstadoEnemigoPruebaEstados EstadoActual;

        public EnemigoPruebaEstados()
        {
            EstadoActual = new EstadoIrHaciaDerechaEnemigoPruebaEstado();

        }

        public void Actualizate()
        {
            EstadoActual.Procesate(this);
        }
    }

    class EstadoEnemigoPruebaEstados
    {
        public virtual void Procesate(EnemigoPruebaEstados LLamador)
        {

        }

    }

    class EstadoIrHaciaDerechaEnemigoPruebaEstado : EstadoEnemigoPruebaEstados
    {
        public override void Procesate(EnemigoPruebaEstados Llamador)
        {
            Llamador.Posicion.X += Llamador.Velocidad.X;

            if (Llamador.Posicion.X > 650)
            {
                Llamador.EstadoActual = new EstadoIrHaciaIzquierdaEnemigoPruebaEstados();
            }
        }
    }

    class EstadoIrHaciaIzquierdaEnemigoPruebaEstados : EstadoEnemigoPruebaEstados
    {
        public override void Procesate(EnemigoPruebaEstados Llamador)
        {
            Llamador.Posicion.X -= Llamador.Velocidad.X;

            if (Llamador.Posicion.X < 300)
            {
                Llamador.EstadoActual = new EstadoIrHaciaDerechaEnemigoPruebaEstado();
            }
        }

    }

   
}
