﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;



namespace MonoGameEjemplo
{
    class EnemigoPerseguidor : Sprite
    {

        GraphicsDeviceManager graphics;

        public EstadoEnemigoPerseguidor EstadoActual;


        public EnemigoPerseguidor()
        {
            EstadoActual = new PerseguirHeroe();
        }

        public class EstadoEnemigoPerseguidor
        {

            public virtual void Procesate(EnemigoPerseguidor LLamador, Personaje personaje, Sprite Casa)
            {

            }
        }

        public /*void actualizate()*/ void actualizate(EnemigoPerseguidor Llamador, Personaje personaje, Sprite Casa)
        {
            EstadoActual.Procesate(this, personaje, Casa);
        }

        class PerseguirHeroe : EstadoEnemigoPerseguidor
        {
            public override void Procesate(EnemigoPerseguidor Llamador, Personaje personaje, Sprite Casa)
            {

                if (!Casa.Colision_BoundingBox(personaje, Casa)) {

                    if (personaje.Posicion.X < Llamador.Posicion.X)
                    {
                        Llamador.Velocidad.X -= Llamador.Posicion.X;
                    }
                    if (personaje.Posicion.X > Llamador.Posicion.X)
                    {
                        Llamador.Velocidad.X += Llamador.Aceleracion.X;
                    }
                    if (personaje.Posicion.Y < Llamador.Posicion.Y)
                    {
                        Llamador.Velocidad.Y -= Llamador.Aceleracion.Y;
                    }
                    if (personaje.Posicion.Y > Llamador.Posicion.Y)
                    {
                        Llamador.Velocidad.Y += Llamador.Aceleracion.Y;
                    }

                    if (Llamador.Velocidad.X > 2)
                        Llamador.Velocidad.X = 2;
                    if (Llamador.Velocidad.X < -2)
                        Llamador.Velocidad.X = -2;
                    if (Llamador.Velocidad.Y < -2)
                        Llamador.Velocidad.Y = -2;
                    if (Llamador.Velocidad.Y > 2)
                        Llamador.Velocidad.Y = 2;


                    Llamador.Posicion.X += Llamador.Velocidad.X;
                    Llamador.Posicion.Y += Llamador.Velocidad.Y;
                }
                else
                {
                    Llamador.EstadoActual = new RegresarBase();
                }
            }
        }

       

        class RegresarBase : EstadoEnemigoPerseguidor
        {
            public override void Procesate(EnemigoPerseguidor Llamador, Personaje personaje, Sprite Casa)
            {
                if(Casa.Colision_BoundingBox(personaje, Casa))
                {
                    if (Llamador.OrigenEnemy.X > Llamador.Posicion.X)
                    {
                        Llamador.Velocidad.X += Llamador.Aceleracion.X;
                    }
                    if(Llamador.OrigenEnemy.X < Llamador.Posicion.X)
                    {
                        Llamador.Velocidad.X -= Llamador.Aceleracion.X;
                    }

                    if (Llamador.OrigenEnemy.Y < Llamador.Posicion.Y)
                    {
                        Llamador.Velocidad.Y -= Llamador.Aceleracion.Y;
                    }
                    if (Llamador.OrigenEnemy.Y > Llamador.Posicion.Y)
                    {
                        Llamador.Velocidad.Y += Llamador.Aceleracion.Y;
                    }
                    

                    if (Llamador.Velocidad.X > 2)
                        Llamador.Velocidad.X = 2;
                    if (Llamador.Velocidad.X < -2)
                        Llamador.Velocidad.X = -2;

                    if (Llamador.Velocidad.Y < -2)
                        Llamador.Velocidad.Y = -2;
                    if (Llamador.Velocidad.Y > 2)
                        Llamador.Velocidad.Y = 2;

                
                    Llamador.Posicion.X += Llamador.Velocidad.X;
                    Llamador.Posicion.Y += Llamador.Velocidad.Y;
                    
                }
                else
                {
                    Llamador.EstadoActual = new PerseguirHeroe();
                }
            }
        }


        /*
        public void Actualizar(Sprite Personaje)
        {

            if (Personaje.Posicion.X < Posicion.X)
            {
                Velocidad.X -= Aceleracion.X;
            }
            if (Personaje.Posicion.X > Posicion.X)
            {
                Velocidad.X += Aceleracion.X;
            }
            if (Personaje.Posicion.Y < Posicion.Y)
            {
                Velocidad.Y -= Aceleracion.Y;
            }
            if (Personaje.Posicion.Y > Posicion.Y)
            {
                Velocidad.Y += Aceleracion.Y;
            }

            if (Velocidad.X > 2)
                Velocidad.X = 2;
            if (Velocidad.X < -2)
                Velocidad.X = -2;
            if (Velocidad.Y < -2)
                Velocidad.Y = -2;
            if (Velocidad.Y > 2)
                Velocidad.Y = 2;


            Posicion.X += Velocidad.X;
            Posicion.Y += Velocidad.Y;
        }

        */

        /*
        public void regresa()
        {
            /*Vector2 EstadoInicial;
            EstadoInicial = new Vector2(0, 0);

            EstadoInicial.X += 1;
            //if (EstadoInicial.X > this.graphics.Viewport.Width)
                EstadoInicial.X = 0;
            Posicion += Velocidad;

            if (Posicion.X > 499 || Posicion.X < 501)
            {
               
            }
            if (Posicion.Y > 9 || Posicion.Y < 11)
            {
                Velocidad.Y += -1;
            }
            
            */
    }

  
}
