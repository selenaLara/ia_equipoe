﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace MonoGameEjemplo
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        // pelotita pelotita;
        Personaje principal;
        EnemigoPerseguidor enemy1;
        //EnemigoPruebaEstados prueba_estados;

        Sprite casa;
        


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
          //  pelotita = new pelotita();
            principal = new Personaje();
            enemy1 = new EnemigoPerseguidor();
          //  prueba_estados = new EnemigoPruebaEstados();
            casa = new Sprite();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            var randomX = new Random().Next(130, 650);
            var randomY = new Random().Next(130, 300);
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            //pelotita.Inicializar(Content.Load<Texture2D>("pelotita"), new Vector2(10, 100), new Vector2(5, 3));
            principal.Inicializar(Content.Load<Texture2D>("normal"), new Vector2(600, 200), new Vector2(10, 10));
            enemy1.Inicializar(Content.Load<Texture2D>("veg"), new Vector2(0, 250), new Vector2(0, 0), new Vector2(0.1f, 0.1f));
            //prueba_estados.Inicializar(Content.Load<Texture2D>("enemy2"), new Vector2(100, 200), new Vector2(5, 5));
            casa.Inicializar(Content.Load<Texture2D>("casas"), new Vector2(550, 10));
            
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            //pelotita.Actualizate();
            principal.Actualizar(Keyboard.GetState());
            //enemy1.Actualizar(principal);
            enemy1.actualizate(enemy1,principal,casa);

            if (casa.Colision_BoundingBox(principal, casa))
            {
                Console.WriteLine("Dentro de la casa");
            }
            else
            {
                Console.WriteLine("Fuera de la casa");
            }

            if (principal.ColisionFinal(principal, enemy1))
            {
                Exit();
            }
          
            
            /*
            if (principal.Colisionando(enemy1) == true)
                base.Update(gameTime);

            

            if (principal.Colision_BoundingBox(principal,casa))//si el principal entra a la zona segura (enemy choca con zona segura no puede entrar)
            {
                //enemy1.Actualizar(prueba_estados);
                //enemy1.Inicializar --
                  enemy1.regresa();
            }

            if (principal.Colisionando(casa) == false) //si el principal sale de la zona segura
                {
                enemy1.Actualizate();
                }

    */
            //prueba_estados.Actualizate();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DarkSlateBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            //pelotita.Dibujate(spriteBatch);
            principal.Dibujate(spriteBatch);
            enemy1.Dibujate(spriteBatch);
            //prueba_estados.Dibujate(spriteBatch);
            casa.Dibujate(spriteBatch); //eje X, Y, width, height
            spriteBatch.End();

            base.Draw(gameTime);
        }

        
    }
}

