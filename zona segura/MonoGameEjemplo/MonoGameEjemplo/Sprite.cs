﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGameEjemplo
{
    class Sprite
    {
        public Vector2 Posicion;
        public Vector2 Velocidad;
        protected Vector2 Aceleracion;

        public Vector2 OrigenEnemy;

        protected Texture2D Grafico;



        public void Inicializar(Texture2D Grafico, Vector2 Posicion)
        {
            this.Grafico = Grafico;
            this.Posicion = Posicion;
        }

        public void Inicializar(Texture2D Grafico, Vector2 Posicion, Vector2 Velocidad)
        {
            this.Grafico = Grafico;
            this.Posicion = Posicion;
            this.Velocidad = Velocidad;

           
        }

        public void Inicializar(Texture2D Grafico, Vector2 Posicion, Vector2 Velocidad, Vector2 Aceleracion)
        {
            this.Grafico = Grafico;
            this.Posicion = Posicion;
            this.Velocidad = Velocidad;
            this.Aceleracion = Aceleracion;

            OrigenEnemy = Posicion;
        }

        public void Dibujate(SpriteBatch Dibujador)
        {
            Dibujador.Draw(Grafico, Posicion, Color.White);
        }

        public void Actualizate()
        {
            Posicion.X--;
        }

        public bool habilitado;

        public bool Colisionando(Sprite principal)
        {
            //    Grafico.Width;
            //    Posicion.X;
            //    P.Posicion.X;

            if (principal.Posicion.X + principal.Grafico.Width < Posicion.X)
            {
                return false;
            }

            else if (principal.Posicion.X + principal.Grafico.Width > Posicion.X)
            {
                return false;
            }


            else if (principal.Posicion.Y + principal.Grafico.Height < Posicion.Y)
            {
                return false;
            }

            else if (principal.Posicion.Y + principal.Grafico.Height > Posicion.Y)
            {
                return false;
            }

            return false;
            
        }

        public Boolean Colision_BoundingBox(Sprite principal, Sprite casa)
        {
            Vector3 min1 = new Vector3(principal.Posicion.X, principal.Posicion.Y, 0);
            Vector3 max1 = new Vector3((principal.Posicion.X + principal.Grafico.Width), (principal.Posicion.Y + principal.Grafico.Height), 0);
            Vector3 min2 = new Vector3(casa.Posicion.X, casa.Posicion.Y, 0);
            Vector3 max2 = new Vector3((casa.Posicion.X + casa.Grafico.Width), (casa.Posicion.Y + casa.Grafico.Height), 0);
            // Se Multipla por 0.99 para agregar el 1% de tolerancia
            BoundingBox box1 = new BoundingBox(min1, max1 * new Vector3(1f, 1f, 0));
            BoundingBox box2 = new BoundingBox(min2, max2 * new Vector3(1f, 1f, 0));
            if (box1.Intersects(box2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean ColisionFinal(Sprite principal, Sprite enemy)
        {
            Vector3 min1 = new Vector3(principal.Posicion.X, principal.Posicion.Y, 0);
            Vector3 max1 = new Vector3((principal.Posicion.X + principal.Grafico.Width), (principal.Posicion.Y + principal.Grafico.Height), 0);
            Vector3 min2 = new Vector3(enemy.Posicion.X, enemy.Posicion.Y, 0);
            Vector3 max2 = new Vector3((enemy.Posicion.X + enemy.Grafico.Width), (enemy.Posicion.Y + enemy.Grafico.Height), 0);
            // Se Multipla por 0.99 para agregar el 1% de tolerancia
            BoundingBox box1 = new BoundingBox(min1, max1 * new Vector3(1f, 1f, 0));
            BoundingBox box2 = new BoundingBox(min2, max2 * new Vector3(1f, 1f, 0));
            if (box1.Intersects(box2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
